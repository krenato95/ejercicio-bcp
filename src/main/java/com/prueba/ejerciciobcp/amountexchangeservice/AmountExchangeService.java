package com.prueba.ejerciciobcp.amountexchangeservice;

import com.prueba.ejerciciobcp.dto.AmountExchangeServiceRequest;
import com.prueba.ejerciciobcp.dto.AmountExchangeServiceResponse;
import io.reactivex.Single;

public interface AmountExchangeService {

    Single<AmountExchangeServiceResponse> convertAmount(AmountExchangeServiceRequest request);

}
