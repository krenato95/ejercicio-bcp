package com.prueba.ejerciciobcp.amountexchangeservice;

import com.prueba.ejerciciobcp.dto.ExchangeRateDTO;
import com.prueba.ejerciciobcp.dto.AmountExchangeServiceRequest;
import com.prueba.ejerciciobcp.dto.AmountExchangeServiceResponse;
import com.prueba.ejerciciobcp.mapper.ResponseMapper;
import com.prueba.ejerciciobcp.model.ExchangeRateModel;
import com.prueba.ejerciciobcp.repository.ExchangeRateRepository;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AmountExchangeServiceImpl implements AmountExchangeService {

    @Autowired
    private ExchangeRateRepository exRateRepository;

    @Override
    public Single<AmountExchangeServiceResponse> convertAmount(AmountExchangeServiceRequest request) {

        double amountToConvert = request.getAmount();
        double convertedAmount;
        final String originCurrency = request.getOriginCurrency();
        final String destinyCurrency = request.getDestinyCurrency();

        ExchangeRateDTO exchangeRate;

        exchangeRate = this.settingExchangeRate(originCurrency, destinyCurrency);
        if ("SALE".equals(exchangeRate.getOperationType())) {
            convertedAmount = amountToConvert * exchangeRate.getValue();
        } else {
            convertedAmount = amountToConvert / exchangeRate.getValue();
        }


        return ResponseMapper.settingServiceResponse(amountToConvert,
                                                        convertedAmount,
                                                        originCurrency,
                                                        request.getDestinyCurrency(),
                                                        exchangeRate.getValue()).subscribeOn(Schedulers.io());
    }

    private ExchangeRateDTO settingExchangeRate(String originCurrency, String destinyCurrency) {
        ExchangeRateModel databaseExchangeRate = exRateRepository.findByOriginAndDestinyCurrency(originCurrency, destinyCurrency);

        return new ExchangeRateDTO(databaseExchangeRate.getOperationType(), databaseExchangeRate.getExchangeRateValue());
    }
}
