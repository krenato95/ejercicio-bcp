package com.prueba.ejerciciobcp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AmountExchangeServiceResponse {

    private double amount;
    private double convertedAmount;
    private String originCurrency;
    private String destinyCurrency;
    private double exchangeRate;

}
