package com.prueba.ejerciciobcp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UpdateExRateRequest {

    private String originCurrency;
    private String destinyCurrency;
    private double value;

}
