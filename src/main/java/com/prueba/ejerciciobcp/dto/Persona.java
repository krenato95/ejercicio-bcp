package com.prueba.ejerciciobcp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Persona {

    private String nombre;
    private double sueldo;
}
