package com.prueba.ejerciciobcp.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AmountExchangeServiceRequest {

    private double amount;
    private String originCurrency;
    private String destinyCurrency;

}
