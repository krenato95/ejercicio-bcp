package com.prueba.ejerciciobcp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class ExchangeRateDTO {

    private String operationType;
    private double value;

}
