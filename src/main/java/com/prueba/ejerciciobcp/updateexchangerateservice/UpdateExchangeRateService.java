package com.prueba.ejerciciobcp.updateexchangerateservice;

import com.prueba.ejerciciobcp.dto.UpdateExRateRequest;
import io.reactivex.Completable;

public interface UpdateExchangeRateService {

    Completable updateExRate(UpdateExRateRequest updateExRateRequest);

}
