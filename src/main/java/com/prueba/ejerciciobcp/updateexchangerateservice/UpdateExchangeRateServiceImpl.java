package com.prueba.ejerciciobcp.updateexchangerateservice;

import com.prueba.ejerciciobcp.dto.UpdateExRateRequest;
import com.prueba.ejerciciobcp.repository.ExchangeRateRepository;
import io.reactivex.Completable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateExchangeRateServiceImpl implements UpdateExchangeRateService{

    @Autowired
    private ExchangeRateRepository exRateRepository;

    @Override
    public Completable updateExRate(UpdateExRateRequest updateExRateRequest) {
        exRateRepository.updateExchangeRate(updateExRateRequest.getOriginCurrency(), updateExRateRequest.getDestinyCurrency(), updateExRateRequest.getValue());

        return Completable.complete();
    }
}
