package com.prueba.ejerciciobcp.model;

import org.hibernate.annotations.Generated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "EXCHANGE_RATE")
public class ExchangeRateModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int exchangeRateId;
    private String originCurrency;
    private String destinyCurrency;
    @Column(precision = 2)
    private double exchangeRateValue;
    private String operationType;

}
