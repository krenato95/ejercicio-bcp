package com.prueba.ejerciciobcp.repository;

import com.prueba.ejerciciobcp.model.ExchangeRateModel;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRateModel, Integer> {

    @Query("SELECT u FROM ExchangeRateModel u WHERE u.originCurrency = ?1 AND u.destinyCurrency = ?2")
    ExchangeRateModel findByOriginAndDestinyCurrency(String originCurrency, String destinyCurrency);

    @Modifying
    @Transactional
    @Query("UPDATE ExchangeRateModel u SET u.exchangeRateValue = ?3 WHERE u.originCurrency = ?1 AND u.destinyCurrency = ?2")
    void updateExchangeRate(String originCurrency, String destinyCurrency, double value);
}
