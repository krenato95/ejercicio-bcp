package com.prueba.ejerciciobcp.controller;

import com.prueba.ejerciciobcp.dto.AmountExchangeServiceRequest;
import com.prueba.ejerciciobcp.dto.AmountExchangeServiceResponse;
import com.prueba.ejerciciobcp.amountexchangeservice.AmountExchangeService;
import com.prueba.ejerciciobcp.dto.Persona;
import com.prueba.ejerciciobcp.dto.UpdateExRateRequest;
import com.prueba.ejerciciobcp.updateexchangerateservice.UpdateExchangeRateService;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/convert-service")
public class ServiceController {

    @Autowired
    private AmountExchangeService amountExchangeService;

    @Autowired
    private UpdateExchangeRateService updateExchangeRateService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/convert-amount", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<AmountExchangeServiceResponse> convertAmount(@RequestBody AmountExchangeServiceRequest exchangeServiceRequest) {
        return amountExchangeService.convertAmount(exchangeServiceRequest)
                        .subscribeOn(Schedulers.io());
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/update-exchange-rate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Completable updateExchangeRate(@RequestBody UpdateExRateRequest updateExRateRequest) {
        return updateExchangeRateService.updateExRate(updateExRateRequest);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/most-salary", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Persona getMostSalary(@RequestBody List<Persona> personaList) {
        Persona personaConMasSueldo = new Persona();
        personaConMasSueldo.setSueldo(0.00);
        for (int i = 0; i < personaList.size(); i++) {
            double personaSueldo = personaList.get(i).getSueldo();
            if(personaSueldo > personaConMasSueldo.getSueldo()) {
                personaConMasSueldo.setSueldo(personaSueldo);
                personaConMasSueldo.setNombre(personaList.get(i).getNombre());
            }
        }
        return personaConMasSueldo;
    }
}
