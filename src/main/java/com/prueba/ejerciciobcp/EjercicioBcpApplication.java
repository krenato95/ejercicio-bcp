package com.prueba.ejerciciobcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioBcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioBcpApplication.class, args);
	}

}
