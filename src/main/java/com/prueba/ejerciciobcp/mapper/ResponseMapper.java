package com.prueba.ejerciciobcp.mapper;

import com.prueba.ejerciciobcp.dto.AmountExchangeServiceResponse;
import io.reactivex.Single;

public class ResponseMapper {

    private ResponseMapper() {}

    public static Single<AmountExchangeServiceResponse> settingServiceResponse(double amountToConvert,
                                                                               double convertedAmount,
                                                                               String originCurrency,
                                                                               String destinyCurrency,
                                                                               double exchangeRateValue) {

        AmountExchangeServiceResponse response = new AmountExchangeServiceResponse();
        response.setAmount(amountToConvert);
        response.setConvertedAmount(convertedAmount);
        response.setOriginCurrency(originCurrency);
        response.setDestinyCurrency(destinyCurrency);
        response.setExchangeRate(exchangeRateValue);

        return Single.just(response);
    }
}
